const { Router } = require('express')
const config = require('config')
const shortid = require('shortid')
const Link = require('../models/Link')
const authMiddleware = require('../middleware/auth.middleware')

const router = Router()

router.post('/generate', authMiddleware, async (req, res) => {
    try {
        const baseUrl = config.get('baseUrl')
        const { from } = req.body
        
        const existedLink = await Link.findOne({ from })

        if (existedLink) {
            return res.json({ link: existedLink })
        }

        const code = shortid.generate()
        const to = baseUrl + '/t/' + code

        const link = new Link({ code, to, from, owner: req.user.userId })
        await link.save()

        res.status(201).json(link)
    } catch (e) {
        res.status(500).json({ message: 'Something went wrong, try again!' })
    }
})

router.get('/', authMiddleware, async (req, res)  => {
    try {
        const links = await Link.find({ owner: req.user.userId }) // Поле user в req формируется в authMiddleware

        res.status(200).json(links)
    } catch (e) {
        res.status(500).json({ message: 'Something went wrong, try again!' })
    }
})

router.get('/:id', authMiddleware, async (req, res) => {
    try {
        const links = await Link.findById(req.params.id)

        res.status(200).json(links)
    } catch (e) {
        res.status(500).json({ message: e.message || 'Something went wrong, try again!' })
    }
})

module.exports = router
