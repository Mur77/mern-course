import React, { useCallback, useContext, useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'

import { useHttp } from '../hooks/http.hook'
import { AuthContext } from '../context/AuthContext'
import { Loading } from '../components/Loading'
import { LinkCard } from '../components/LinkCard'

export const DetailPage = () => {
    const [link, setLink] = useState(null)
    const linkId = useParams().id
    const { request, loading } = useHttp()
    const { token } = useContext(AuthContext)

    const getLink = useCallback( async () => {
        try {
            const data = await request(`api/link/${linkId}`, 'GET', null, {
                Authorization: `Bearer ${token}`
            })
            setLink(data)
        } catch (e) {}
    }, [token, linkId, request])

    useEffect(() => {
        getLink()
    }, [getLink])

    if (loading) {
        return <Loading />
    }

    return (
        <>
            { !loading && link && <LinkCard link={link} /> }
        </>
    )
}
