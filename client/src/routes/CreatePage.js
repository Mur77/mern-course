import React, { useState, useEffect, useContext } from 'react'
import { useNavigate } from 'react-router-dom'

import { AuthContext } from '../context/AuthContext'
import { useHttp } from '../hooks/http.hook'

export const CreatePage = () => {
    const auth = useContext(AuthContext)
    const navigate = useNavigate()
    const [link, setLink] = useState()
    const { request } = useHttp()

    const handleChange = (e) => {
        setLink(e.target.value)
    }

    const handleKeyDown = async (e) => {
        if (e.key === 'Enter') {
            try {
                const data = await request('api/link/generate', 'POST', { from: link }, {
                    Authorization: `Bearer ${auth.token}`
                })
                navigate(`/detail/${data.link._id}`)
            } catch (e) {

            }
        }
    }

    useEffect(() => {
        window.M.updateTextFields()
    }, [])

    return (
        <div className='row'>
            <div className='col s8 offset-s2' style={{ paddingTop: '2rem' }}>
                <div className='input-field'>
                    <input 
                        placeholder='Вставьте ссылку'
                        id="link"
                        type="text"
                        value={link}
                        onChange={handleChange}
                        onKeyDown={handleKeyDown}
                    />
                    <label htmlFor="link">Введите ссылку</label>
                </div>
            </div>
        </div>
    )
}
