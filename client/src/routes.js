import React from "react"
import { Route, Routes } from 'react-router-dom'

import { LinksPage } from './routes/LinksPage'
import { CreatePage } from './routes/CreatePage'
import { DetailPage } from './routes/DetailPage'
import { AuthPage } from './routes/AuthPage'

export const useRoutes = (isAuthenticated) => {
    if (isAuthenticated) {
        return (
            <Routes>
                <Route path="links" element={<LinksPage />} />
                <Route path="detail/:id" element={<DetailPage />} />
                <Route path="*" element={<CreatePage />} /> 
            </Routes>
        )
    }

    return (
        <Routes>
            <Route path="*" element={<AuthPage />} />
        </Routes>
    )
}
