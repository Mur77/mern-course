import React from "react"
import { Link } from 'react-router-dom'

export const LinksList = ({ links }) => {
    if (!links.length) {
        return <p className="center">Ссылок пока нет</p>
    }

    return (
        <table>
            <thead>
                <tr>
                    <th>№</th>
                    <th>Оригинальная</th>
                    <th>Сокращенная</th>
                    <th>Дата</th>
                    <th>Клики</th>
                    <th>Открыть</th>
                </tr>
            </thead>
            <tbody>
                {links.map((item, index) => (
                    <tr key={item._id}>
                        <td>{index+1}</td>
                        <td>{item.from}</td>
                        <td>{item.to}</td>
                        <td>{new Date(item.date).toLocaleDateString()}</td>
                        <td>{item.clicks}</td>
                        <td><Link to={`/detail/${item._id}`}>Открыть</Link></td>
                    </tr>
                ))}
            </tbody>
    </table>
    )
}
