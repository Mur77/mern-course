import React from "react"
import { BrowserRouter } from "react-router-dom"

import { useRoutes } from "./routes"
import { useAuth } from "./hooks/auth.hook"
import { AuthContext } from "./context/AuthContext"
import { Navbar } from "./components/Navbar"
import { Loading } from './components/Loading'

import 'materialize-css'
import "./index.css"

const App = () => {
    const { login, logout, token, userId, ready } = useAuth()
    const isAuthenticated = !!token

    const routes = useRoutes(isAuthenticated)

    if (!ready) {
        return <Loading />
    }

    return(
        <AuthContext.Provider value={{
            login, logout, token, userId, isAuthenticated
        }}>
            <BrowserRouter>
                {isAuthenticated && <Navbar /> }
                <div className="container">
                    {routes}
                </div>
            </BrowserRouter>
        </AuthContext.Provider>
    )
}

export default App
